.data 0x10000000
.align 0
ch1: .byte 'a' 
word1: .word 0x89abcdef
ch2:.byte 'b'
word2: .word 0

.text
.globl main
main: 

lui $a0, 4096    #load the address of word1 to $a0
ori $a0, $a0, 1

lui $a1, 4096   #load the address of word2 to $a1
ori $a1, $a1, 6

lwl $t0,3($a0)
lwr $t0,0($a0)
swl $t0,3($a1)
swr $t0,0($a1)
jr $ra
