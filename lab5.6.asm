.data
user1:.word 0
message1: .asciiz "please input a integer:"
message2: .asciiz "If bytes were layed in reverse order the number would be:"
.text
.globl main
main:
addi $sp,$sp,-4
sw $ra,4($sp)
li $v0,4
la $a0, message1
syscall
li $v0,5
syscall
sw $v0,user1
la $a0,user1
jal Reverse_bytes
li $v0,4
la $a0, message2
syscall 
li $v0,1
lw $a0,user1
syscall
lw $ra,4($sp)
addi $sp,$sp,4
jr $ra

Reverse_bytes:
lb $t0,0($a0)
lb $t1,1($a0)
lb $t2,2($a0)
lb $t3,3($a0)
sb $t0,3($a0)
sb $t1,2($a0)
sb $t2,1($a0)
sb $t3,0($a0)
jr $ra