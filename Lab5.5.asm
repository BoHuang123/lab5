.data 0x10000000
char1: .byte 'a' 
double1: .double 1.1
char2: .byte 'b'
half1: .half 0x8001
char3: .byte 'c'
word1: .word 0x56789abc
char4: .byte 'd'
word2: .word 0
.text
.globl main
main: 
lui $a0, 0x1000
ori $a0, 0x0018
lb $t0,3($a0)
lb $t1,2($a0)
lb $t2,1($a0)
lb $t3,0($a0)
lbu $t4,3($a0)
lbu $t5,2($a0)
lbu $t6,1($a0)
lbu $t7,0($a0)
lh $t8,half1
lhu $t9,half1
la $a0,word2
sb $t3,3($a0)
sb $t2,2($a0)
sb $t1,1($a0)
sb $t0,0($a0)
#lw $t4,word2
jr $ra
